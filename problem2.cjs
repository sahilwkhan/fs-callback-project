module.exports = processTextFile;

const { log } = require("console");
// Import fs (File system) Module
const fs = require("fs");
const path = require("path")

function processTextFile(txtFilePath, fileNamesPath) {
    
    let upperCasePath =  "./upperCaseText.txt";
    let lowerCasePath =  "./lowerCaseText.txt";
    let sortedFilePath = "./sortedText.txt";

    fs.readFile(txtFilePath, 'utf8', (error1, textData) => {
        if (error1) {
            return console.log(error1);
        }
        else {

            // Read the text file and convert to uppercase
            let upperCaseContent = textData.toUpperCase();

            fs.writeFile(upperCasePath , upperCaseContent, function (error2) {
                if (error2) {
                    return console.log("Error writing file", error2);
                }
                else {
                    console.log(`New file with Upper Case text created successfully.`);

                    let upperCaseFileName = upperCasePath.slice(upperCasePath.indexOf("/") + 1);
                    fs.appendFile(fileNamesPath, upperCaseFileName + "\n", (error3) => {
                        if (error3) {
                            return console.log(error3);
                        }
                        else {
                            console.log(`${upperCaseFileName} added to filenames.txt`);
                            
                            // Read the uppercase file and convert to lowercase with split sentences.

                            fs.readFile(upperCasePath, 'utf8', (error4, lowerCaseData) => {
                                if (error4) {
                                    return console.log(error4);
                                }
                                else {
                                    let lowerCaseContent = lowerCaseData.toLowerCase().replaceAll(". ", ".\n");

                                    fs.writeFile(lowerCasePath, lowerCaseContent, function (error5) {
                                        if (error5) {
                                            return console.log("Error writing file", error5);
                                        }
                                        else {
                                            console.log(`New file with lower Case text and each sentence on newline is created successfully.`);
                                            
                                            let lowerCaseFileName = lowerCasePath.slice(lowerCasePath.indexOf("/") + 1);
                                            fs.appendFile(fileNamesPath, lowerCaseFileName + "\n", (error6) => {
                                                if (error6) {
                                                    return console.log(error6);
                                                }
                                                else {
                                                    console.log(`${lowerCaseFileName} added to filenames.txt`);


                                                    // Read the lowercase file and convert to sorted txt
                                                    fs.readFile(lowerCasePath, 'utf8', (error7, lowerCaseData) => {
                                                        if (error7) {
                                                            return console.log(error7);
                                                        }
                                                        else {
                                                            let sortedContent = lowerCaseData.split("\n").sort();
        
                                                            fs.writeFile(sortedFilePath, JSON.stringify(sortedContent), function (error8) {
                                                                if (error8) {
                                                                    return console.log("Error writing file", error8);
                                                                }
                                                                else {
                                                                    console.log(`New file with sorted text is created successfully.`);

                                                                    let sortedTextFielName = sortedFilePath.slice(sortedFilePath.indexOf("/") + 1);

                                                                    fs.appendFile(fileNamesPath, sortedTextFielName + "\n", (error9) => {
                                                                        if (error9) {
                                                                            return console.log(error9);
                                                                        }
                                                                        else {
                                                                            console.log(`${sortedTextFielName} added to filenames.txt`);

                                                                            // Remove all new files
                                                                            let listOfFiles = fs.readFile("./filenames.txt", 'utf8', (error7, namesOfTheFiles) => {
                                                                                if (error7) {
                                                                                    return console.log(error7);
                                                                                }

                                                                                else {
                                                                                    for (let file of namesOfTheFiles.split("\n")) {
                                                                                        // Check if the file to be deleted exists or not
                                                                                        fs.stat(file, (err, stats) => {
                                                                                            if (err) {
                                                                                                return console.log('File does not exist');
                                                                                            } else {
                                                                                                console.log('File exists');
                                                                                                fs.unlink(file, (error10) => {
                                                                                                    if (error10) {
                                                                                                        console.log("The files to be deleted does not exist.");
                                                                                                        return console.log(error10);
                                                                                                    }
                                                                                                    else {
                                                                                                        console.log(`${file} is successfully deleted.`);
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                        });
                                                                                        
                                                                                    };
                                                                                };
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });


                                                }
                                            })


                                        }
                                    });
                                }
                            });


                        }
                    })

                }
            });

        }
    
        
    });
}